@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                <h5>Genero: {{$genero}}</h5>
                </div>
                <div class="panel-body">
                    <h2 class="tituloAdmin">{{$pelicula}}</h2>
                    <a href="{{ url('/') }}"><button class="btn btn-success btn-perfil btn-right">Volver</button></a>
                </div> 
            </div>
        </div>
    </div>
</div>
@endsection