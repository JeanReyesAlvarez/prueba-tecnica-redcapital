@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Genero de peliculas</div>

                <div class="panel-body">
                    <a href="{{url('/pelicula/'."Drama")}}"><button onclick="" class="btn btn-info btn-center">Drama</button></a>
                    <a href="{{url('/pelicula/'."Comedia")}}"><button onclick="" class="btn btn-info btn-center">Comedia</button></a>
                    <a href="{{url('/pelicula/'."Accion")}}"><button onclick="" class="btn btn-info btn-center">Acción</button></a>
                    <a href="{{url('/pelicula/'."Terror")}}"><button onclick="" class="btn btn-info btn-center">Terror</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
