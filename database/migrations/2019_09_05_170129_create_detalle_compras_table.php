<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('detalle_compras');
        Schema::create('detalle_compras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('compra_id')->unsigned();
            $table->foreign('compra_id')->references('id')->on('compras');
            $table->string('Nombre');
            $table->string('Precio');
            $table->integer('Categoria_id')->unsigned();
            $table->foreign('Categoria_id')->references('id')->on('categorias');
            $table->timestamps();
        });
    }
    
    $table->foreign('idglaseado')->references('idglaseado')->on('glaseado');
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_compras');
    }
}
