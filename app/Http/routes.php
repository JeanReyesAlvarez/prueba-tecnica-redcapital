<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::auth();

Route::get('/home', 'HomeController@index');

// Route::get('/precio', 'comprasController@total_compra_por_categoria');
// Route::get('/compra', 'comprasController@seleccionar_compra');

//ruta que muestra peliculas de distinto genero
Route::get('/pelicula/{genero}', 'peliculasController@mostrar_peliculas');



