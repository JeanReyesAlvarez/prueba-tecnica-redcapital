<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class peliculasController extends Controller
{
    public function __construct()
    {
        
    }

    public function mostrar_peliculas($genero){
        if ($genero == "Drama"){
            $genero = "Drama";
            $pelicula = "Bohemian Rhapsody";
            return view('peliculas.index',["pelicula"=>$pelicula, "genero"=>$genero]);
        }elseif($genero == "Comedia"){
            $genero = "Comedia";
            $pelicula = "La Máscara";
            return view('peliculas.index',["pelicula"=>$pelicula, "genero"=>$genero]);
        }elseif($genero == "Accion"){
            $genero = "Acción";
            $pelicula = "Fast & Furious";
            return view('peliculas.index',["pelicula"=>$pelicula, "genero"=>$genero]);
        }elseif($genero == "Terror"){
            $genero = "Terror";
            $pelicula = "Anabelle";
            return view('peliculas.index',["pelicula"=>$pelicula, "genero"=>$genero]);
        }else{
            return response()->view('errors.404', [], 404);
        }
    }
}
