<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Compra;
use App\Categoria;
use App\Detalle_compra;

use DB;
use Carbon\Carbon;
use Auth;
use App\Http\Requests;

class comprasController extends Controller
{
    public function __construct()
    {
        
    }

    public function seleccionar_compra(){
        //Muestra todas las compras con sus detalles asociados
        $compras = Detalle_compra::join('compras','detalle_compras.compra_id','=','compras.id')
        ->join('categorias','detalle_compras.Categoria_id','=','categorias.id')
        ->select(DB::raw("detalle_compras.compra_id, detalle_compras.Nombre as nombreDetalle, detalle_compras.Precio, categorias.Nombre as categoria"))
        ->groupBy(DB::raw("detalle_compras.id"))
        ->get();

        return($compras);
        
    }

    public function total_compra_por_categoria(){
        //Muestra suma total de cada compra, ordenada por categoria
        $precioPorCategoria = Detalle_compra::join('compras','detalle_compras.compra_id','=','compras.id')
        ->select(DB::raw("SUM(detalle_compras.Precio) as precioTotal"))
        ->groupBy(DB::raw("detalle_compras.compra_id"))
        ->orderBy(DB::raw("detalle_compras.Categoria_id"))
        ->get();
        return($precioPorCategoria);
    }
}
